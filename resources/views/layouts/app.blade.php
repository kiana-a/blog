<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <meta name="keywords" content=""/>
    <meta name="description" content=""/>
    <link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600,700,900" rel="stylesheet"/>
    <link href="css/default.css" rel="stylesheet"/>
    <link href="css/fonts.css" rel="stylesheet"/>

</head>
<body>
<div id="header-wrapper">
    <div id="header" class="container">
        <div id="logo">
            <h1><a href="/">Kiana's Online Shop</a></h1>
        </div>
        <div id="menu">
            <ul>
                <li class="{{Request::path() === '/' ? 'current_page_item' : ''}}"><a href="/" accesskey="1" title="">Homepage</a></li>
                <li class="{{Request::path() === '/' ? 'current_page_item' : ''}}"><a href="/products" accesskey="2" title="">Our Products</a></li>
                <li class="{{Request::path() === '/' ? 'current_page_item' : ''}}"><a href="/about-us" accesskey="3" title="">About Us</a></li>
                <li class="{{Request::path() === '/' ? 'current_page_item' : ''}}"><a href="/register" accesskey="4" title="">Register</a></li>
                <li class="{{Request::path() === '/' ? 'current_page_item' : ''}}"><a href="/products/create" accesskey="5" title="">Create Product</a></li>
            </ul>
        </div>
    </div>
    @yield('header')
</div>
@yield('content')
</body>
</html>
