@extends('layouts.app')
@section('content')

    <div id="wrapper">
        <div id="page" class="container">
            <div id="content">
                <div class="title">
                    <h2>Welcome to Kiana's website</h2>
                    <span class="byline">Mauris vulputate dolor sit amet nibh</span></div>
                <div class="container">
                    <h1 class='mt-5'>
                        @foreach($products as $products)
                            <div>
                                <a href={{"/products/".$products->id}}>{{$products->description}}</a>
                            </div>
                            <h1 class="mt-5">
                                {{$products->name}}
                            </h1>
                    </h1>
                    @endforeach
                </div>

            </div>
        </div>
    </div>

@endsection



{{--@extends('layouts.app')--}}


{{--@section('content')--}}
{{--    <div class="container">--}}
{{--        <h1 class='mt-5'>--}}
{{--            @foreach($products as $products)--}}
{{--                <div>--}}
{{--                    <a href={{"/products/".$products->id}}>{{$products->description}}</a>--}}
{{--                </div>--}}
{{--                <h1 class="mt-5">--}}
{{--                    {{$products->name}}--}}
{{--                </h1>--}}
{{--        </h1>--}}
{{--        @endforeach--}}
{{--    </div>--}}
{{--@endsection--}}
