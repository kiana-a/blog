@extends('layouts.app')


@section('content')

    <div id="wrapper">
        <div id="page" class="container">
            <div id="content">
                <div class="title">
                    <h2>Welcome to Kiana's website</h2>
                    <span class="byline">Mauris vulputate dolor sit amet nibh</span></div>
                <div class="container">
                    <div class="card mt-5">
                        <div class="card-body">
                            <h5 class="card-title">
                                Create Product
                            </h5>
                            <form method="post" action="{{route('products.store')}}">
                                {{csrf_field()}}
                                <div class="form-group">
                                    <label for="name">Name</label>
                                    <input type="text" class="form-control" name="name" id="name" value="{{ old('name') }}"
                                           placeholder="name">
                                </div>

                                <div class="form-group">
                                    <label for="description">Description</label>
                                    <input type="text" class="form-control" name="description" id="description"
                                           value="{{ old('description') }}"
                                           placeholder="description">
                                </div>
                                <div class="form-group">
                                    <label for="price">Price</label>
                                    <input type="number" class="form-control" name="price" id="price" value="{{ old('price') }}"
                                           placeholder="price">
                                </div>
                                <div class="form-group">
                                    <label for="weight">Weight</label>
                                    <input type="number" class="form-control" name="weight" id="weight" value="{{ old('weight') }}"
                                           placeholder="weight">
                                </div>
                                @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                                <button type="submit" class="btn btn-success">Save</button>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection



{{--@section('content')--}}
{{--    <div class="container">--}}
{{--        <div class="card mt-5">--}}
{{--            <div class="card-body">--}}
{{--                <h5 class="card-title">--}}
{{--                    Create Product--}}
{{--                </h5>--}}
{{--                <form method="post" action="{{route('products.store')}}">--}}
{{--                    {{csrf_field()}}--}}
{{--                    <div class="form-group">--}}
{{--                        <label for="name">Name</label>--}}
{{--                        <input type="text" class="form-control" name="name" id="name" value="{{ old('name') }}"--}}
{{--                               placeholder="name">--}}
{{--                    </div>--}}

{{--                    <div class="form-group">--}}
{{--                        <label for="description">Description</label>--}}
{{--                        <input type="text" class="form-control" name="description" id="description"--}}
{{--                               value="{{ old('description') }}"--}}
{{--                               placeholder="description">--}}
{{--                    </div>--}}
{{--                    <div class="form-group">--}}
{{--                        <label for="price">Price</label>--}}
{{--                        <input type="number" class="form-control" name="price" id="price" value="{{ old('price') }}"--}}
{{--                               placeholder="price">--}}
{{--                    </div>--}}
{{--                    <div class="form-group">--}}
{{--                        <label for="weight">Weight</label>--}}
{{--                        <input type="number" class="form-control" name="weight" id="weight" value="{{ old('weight') }}"--}}
{{--                               placeholder="weight">--}}
{{--                    </div>--}}
{{--                    @if ($errors->any())--}}
{{--                        <div class="alert alert-danger">--}}
{{--                            <ul>--}}
{{--                                @foreach ($errors->all() as $error)--}}
{{--                                    <li>{{ $error }}</li>--}}
{{--                                @endforeach--}}
{{--                            </ul>--}}
{{--                        </div>--}}
{{--                    @endif--}}
{{--                    <button type="submit" class="btn btn-success">Save</button>--}}
{{--                </form>--}}
{{--            </div>--}}
{{--        </div>--}}

{{--    </div>--}}

{{--@endsection()--}}

