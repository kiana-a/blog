@extends('layouts.app')
@section('content')

    <div id="wrapper">
        <div id="page" class="container">
            <div id="content">
                <div class="title">
                    <h2>Welcome to Kiana's website</h2>
                    <span class="byline">Mauris vulputate dolor sit amet nibh</span></div>

                <div class="container">
                    <h1 class='mt-5'>
                        {{$product->name}}
                    </h1>
                    <p>
                        description : {{$product->description}}
                    </p>
                    <p>
                        weight : {{$product->weight}}
                    </p>
                    <p>
                        price : {{$product->price}}$
                    </p>
                </div>

            </div>
        </div>
    </div>

@endsection






