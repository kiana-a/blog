<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function index()
    {
//        return view("welcome", compact('names'));
        return view("welcome");

    }
    public function __construct()
    {
        $this->middleware('auth')->except('welcom','aboutUs','index');
    }

    public function aboutUs()
    {
        return view("aboutUs");
    }

}
