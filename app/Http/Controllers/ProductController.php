<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth')->only('create');
    }

    public function index()
    {

        $products = Product::all();

        return view('products.index', compact('products'));
    }

    public function show($id)
    {

        $product = Product::findOrFail($id);

        return view('products.show', compact('product'));
    }

    public function create()
    {

        return view('products.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'description' => 'required',
            'weight' => 'required|numeric',
            'price' => 'required|numeric'

        ]);
        dd('after validation');

        Auth::user()->products()->create($request->except('_token'));
        $product = Product::create($request->except('_token'));

    }
}
